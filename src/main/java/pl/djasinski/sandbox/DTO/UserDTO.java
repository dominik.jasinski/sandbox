package pl.djasinski.sandbox.DTO;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserDTO {

    private Long id;

    private String login;

    private boolean activated;

    private String email;

    private String firstName;

    private String lastName;

    private LocalDateTime resetPasswordKeyGenerationDate;

    private LocalDateTime lastPasswordChangeDate;

    private int badLoginCount = 0;

    private boolean blocked;
}
