package pl.djasinski.sandbox.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import pl.djasinski.sandbox.constants.Constants;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user", schema = "public")
public class User extends AbstractAuditingEntity implements Serializable {

    @NotNull
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Setter(AccessLevel.NONE)
    @Column(length = 50, unique = true, updatable = false, nullable = false)
    private String login;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    @Column(name = "password_hash", length = 60)
    private String passwordHash;

    @NotNull
    @Column(nullable = false, columnDefinition = "boolean default false")
    private boolean activated;

    @Size(max = 100)
    @Email
    @Column(length = 100, unique = true)
    private String email;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Size(min = 2, max = 5)
    @Column(name = "lang_key", length = 5)
    private String langKey;

    @Size(max = 100)
    @Column(name = "activation_key", length = 100)
    @JsonIgnore
    private String activationKey;

    @Size(max = 100)
    @Column(name = "reset_password_key", length = 100)
    private String resetPasswordKey;

    @Column(name = "reset_password_key_generation_date")
    private LocalDateTime resetPasswordKeyGenerationDate;

    @Column(name = "last_password_change_date")
    private LocalDateTime lastPasswordChangeDate;

    @Column(name = "bad_login_count", columnDefinition = "integer default 0")
    private int badLoginCount = 0;

    @Column(columnDefinition = "boolean default false")
    private boolean blocked;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "user_authority",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
    private Set<Authority> authorities = new HashSet<>();

    public User(String login, String passwordHash, String email, String firstName, String lastName, String langKey, String activationKey, Set<Authority> authorities) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.langKey = langKey;
        this.activationKey = activationKey;
        this.authorities = authorities;
    }

    public User(String login, String passwordHash, Set<Authority> authorities) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.activated = true;
        this.authorities = authorities;
    }
}
