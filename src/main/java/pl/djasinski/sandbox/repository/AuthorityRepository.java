package pl.djasinski.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.djasinski.sandbox.model.Authority;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, String> {

}
