package pl.djasinski.sandbox.enums;

import lombok.Getter;

@Getter
public enum RoleTypeEnum {
    USER("ROLE_USER"),
    ADMIN("ROLE_ADMIN"),
    ANONYMOUS("ROLE_ANONYMOUS");

    private final String roleName;

    RoleTypeEnum(String roleName) {
        this.roleName = roleName;
    }
}
