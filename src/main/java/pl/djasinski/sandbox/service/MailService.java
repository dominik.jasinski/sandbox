package pl.djasinski.sandbox.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import pl.djasinski.sandbox.controller.request.MailRequest;
import pl.djasinski.sandbox.model.User;

import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

@Log4j2
@Service
public class MailService implements IMailService {

    private final JavaMailSenderImpl javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    /**
     * System default email address that sends the e-mails.
     */
    @Value("${application.mail.from}")
    private String from;

    public MailService(JavaMailSenderImpl javaMailSender, MessageSource messageSource, SpringTemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    @Override
    public void sendActivationEmail(User user, String baseUrl) {
        log.debug("Sending activation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag("pl");
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("mails/activation-email", context);
        String subject = messageSource.getMessage("email.activation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    @Override
    public void sendResetPasswordUrl(User user, String resetUrl) {
        log.debug("Sending reset password e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag("pl");
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("reset_url", resetUrl);
        String content = templateEngine.process("mails/reset-password", context);
        String subject = messageSource.getMessage("email.password.reset.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Override
    public void sendRequest(MailRequest mailRequest) {
        log.debug("Send request message by " + mailRequest.getUser() + ", " + mailRequest.getEmail());
        Locale locale = Locale.forLanguageTag("pl");
        Context context = new Context(locale);
        context.setVariable("content", mailRequest.getContent());
        context.setVariable("user", mailRequest.getUser());
        context.setVariable("email", mailRequest.getEmail());
        String content = templateEngine.process("mails/user-request-message", context);
        String subject = messageSource.getMessage("email.contact.title", null, locale);

        sendEmail(mailRequest.getEmail(), subject, content, false, true);
    }

    @Async
    @Override
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug(
                "Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
                isMultipart,
                isHtml,
                to,
                subject,
                content
        );

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(from);
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent e-mail to User '{}'", to);
        } catch (Exception e) {
            log.warn("E-mail could not be sent to user '{}', exception is: {}", to, e.getMessage());
        }
    }
}
