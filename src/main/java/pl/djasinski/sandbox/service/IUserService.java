package pl.djasinski.sandbox.service;

import pl.djasinski.sandbox.DTO.UserDTO;
import pl.djasinski.sandbox.controller.request.RegisterRequest;
import pl.djasinski.sandbox.controller.request.ResetPasswordRequest;
import pl.djasinski.sandbox.controller.response.AuthenticationResponse;

public interface IUserService extends CrudService<UserDTO, Long> {

    AuthenticationResponse authenticate(String username, String password);

    void registerSysAdmin();

    void registerUser(RegisterRequest customerRegistrationDto);

    void activateUser(String key);

    void resetPassword(String email);

    void resetPassword(ResetPasswordRequest requestDTO);

    void checkRegistrationToken(String token);

}
