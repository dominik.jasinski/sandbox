package pl.djasinski.sandbox.service;

import org.springframework.scheduling.annotation.Async;
import pl.djasinski.sandbox.controller.request.MailRequest;
import pl.djasinski.sandbox.model.User;


public interface IMailService {

    @Async
    void sendActivationEmail(User user, String baseUrl);

    @Async
    void sendResetPasswordUrl(User user, String resetUrl);

    @Async
    void sendRequest(MailRequest mailRequest);

    @Async
    void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml);
}
