package pl.djasinski.sandbox.service;

import com.google.common.collect.Sets;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.djasinski.sandbox.DTO.UserDTO;
import pl.djasinski.sandbox.assembler.common.Assemblers;
import pl.djasinski.sandbox.controller.request.RegisterRequest;
import pl.djasinski.sandbox.controller.request.ResetPasswordRequest;
import pl.djasinski.sandbox.controller.response.AuthenticationResponse;
import pl.djasinski.sandbox.enums.RoleTypeEnum;
import pl.djasinski.sandbox.exception.ApplicationException;
import pl.djasinski.sandbox.exception.ExceptionCode;
import pl.djasinski.sandbox.model.Authority;
import pl.djasinski.sandbox.model.User;
import pl.djasinski.sandbox.repository.AuthorityRepository;
import pl.djasinski.sandbox.repository.UserRepository;
import pl.djasinski.sandbox.security.JwtTokenUtil;
import pl.djasinski.sandbox.security.SecurityUtils;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Log4j2
@Service
public class UserService implements IUserService {

    protected static final String TEST_USER_EMAIL = "cypress.user@mailinator.com";
    protected static final String TEST_USER_ACTIVATION_KEY = "b59add67-aaa5-4024-8234-72081251e28d";

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final IMailService mailService;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;

    @Value("${application.frontend-url}")
    private String frontendUrl;

    @Value("${application.registration.admin-login}")
    private String adminLogin;

    @Value("${application.registration.admin-password}")
    private String adminPassword;

    public UserService(UserRepository userRepository, AuthorityRepository authorityRepository, IMailService mailService, PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.mailService = mailService;
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Override
    public AuthenticationResponse authenticate(String username, String password) {
        UsernamePasswordAuthenticationToken user;

        try {
            user = (UsernamePasswordAuthenticationToken) authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password));
        } catch (BadCredentialsException e) {
            throw new ApplicationException(ExceptionCode.BAD_CREDENTIALS, e.getMessage());
        }

        final UserDetails userDetails = (UserDetails) user.getPrincipal();

        String jwt = jwtTokenUtil.generateToken(userDetails);
        Set<String> roles = SecurityUtils.getRoles(userDetails);

        return new AuthenticationResponse(jwt, roles);
    }

    @Override
    public void registerSysAdmin() {
        userRepository.findOneByLogin(adminLogin).ifPresentOrElse(
                user -> {
                    log.info("SysAdmin account exists!");
                    if (!passwordEncoder.matches(adminPassword, user.getPasswordHash())) {
                        user.setPasswordHash(passwordEncoder.encode(adminPassword));
                        userRepository.save(user);
                        log.info("Password of SysAdmin changed!");
                    }
                }, () -> userRepository.save(new User(
                        adminLogin,
                        passwordEncoder.encode(adminPassword),
                        Sets.newHashSet(new Authority(RoleTypeEnum.ADMIN.getRoleName()))
                ))
        );
    }

    @Override
    @Transactional
    public void registerUser(RegisterRequest request) {
        userRepository.findFirstByLoginOrEmail(request.getLogin(), request.getEmail())
                .map(user -> {
                    if (user.getLogin().equalsIgnoreCase(request.getLogin())) {
                        throw new ApplicationException(ExceptionCode.USER_LOGIN_EXISTS);
                    } else {
                        throw new ApplicationException(ExceptionCode.USER_EMAIL_EXISTS);
                    }
                });

        User newUser = createUser(request);

        userRepository.save(newUser);
        mailService.sendActivationEmail(newUser, frontendUrl);
    }

    @Override
    public void activateUser(String key) {
        userRepository.findOneByActivationKey(key)
                .ifPresentOrElse(user -> {
                    // activate given user for the registration key.
                    user.setActivated(true);
                    user.setActivationKey(null);

                    userRepository.save(user);
                }, () -> {
                    throw new ApplicationException(ExceptionCode.USER_ACTIVATION_BAD_TOKEN);
                });
    }

    @Override
    public void resetPassword(String email) {
        userRepository.findOneByEmail(email).ifPresentOrElse(user -> {
                    String token = UUID.randomUUID().toString();

                    user.setResetPasswordKey(token);
                    user.setResetPasswordKeyGenerationDate(LocalDateTime.now());

                    userRepository.save(user);

                    String url = frontendUrl + String.format("/reset-password/%s/confirm", token);

                    mailService.sendResetPasswordUrl(user, url);
                }, () -> {
                    throw new ApplicationException(ExceptionCode.USER_EMAIL_NOT_EXISTS);
                }
        );
    }

    @Override
    public void resetPassword(ResetPasswordRequest requestDTO) {
        userRepository.findByResetPasswordKey(requestDTO.getToken()).ifPresentOrElse(user -> {
                    user.setPasswordHash(passwordEncoder.encode(requestDTO.getPassword()));
                    user.setLastPasswordChangeDate(LocalDateTime.now());
                    user.setResetPasswordKey(null);

                    userRepository.save(user);
                }, () -> {
            throw new ApplicationException(ExceptionCode.BAD_PASSWORD_RESET_TOKEN);
                }
        );
    }

    @Override
    public void checkRegistrationToken(String token) {
        userRepository.findByResetPasswordKey(token).orElseThrow(
                () -> new ApplicationException(ExceptionCode.BAD_PASSWORD_RESET_TOKEN)
        );
    }

    private User createUser(RegisterRequest request) {
        User newUser = Assemblers.REGISTER_ASSEMBLER.assemblyToDbo(request);

        Optional<Authority> role = Optional.of(authorityRepository.findById(RoleTypeEnum.USER.getRoleName()))
                .orElseGet(() -> Optional.of(authorityRepository.save(new Authority(RoleTypeEnum.USER.getRoleName()))));

        if (role.isPresent()) {
            newUser.setAuthorities(Sets.newHashSet(role.get()));
            newUser.setPasswordHash(passwordEncoder.encode(request.getPassword()));
            var key = UUID.randomUUID().toString();

            if (TEST_USER_EMAIL.equals(newUser.getEmail())) {
                key = TEST_USER_ACTIVATION_KEY;
            }

            newUser.setActivationKey(key);
        }

        return newUser;
    }

    @Override
    public UserDTO getOne(Long id) {
        return Assemblers.USER_ASSEMBLER.assemblyToDto(userRepository.getOne(id));
    }

    @Override
    public List<UserDTO> getAll() {
        return Assemblers.USER_ASSEMBLER.assemblyToDto(userRepository.findAll());
    }

    @Override
    public void add(UserDTO element) {
        //override by UserService::registerUser
    }

    @Override
    public void update(UserDTO element) {
        userRepository.save(Assemblers.USER_ASSEMBLER.assemblyToDbo(element));
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}
