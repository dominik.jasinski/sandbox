package pl.djasinski.sandbox.service;

import java.util.List;

/**
 * Interface UserDataService
 *
 * @author Dominik Jasiński zdjasinski@gmail.com
 */
public interface CrudService<T, I> {

    T getOne(I id);

    List<T> getAll();

    void add(T element);

    void update(T element);

    void delete(I id);
}
