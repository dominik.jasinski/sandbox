package pl.djasinski.sandbox.service;

public interface IUserContextService {

    String getLoggedUserEmail();

    boolean admin();

    boolean user();
}
