package pl.djasinski.sandbox.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pl.djasinski.sandbox.exception.ApplicationException;
import pl.djasinski.sandbox.exception.ExceptionCode;
import pl.djasinski.sandbox.model.User;
import pl.djasinski.sandbox.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Authenticate a user from the database.
 */
@Log4j2
@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String userName) {
        log.debug("Authenticating {}", userName);

        Optional<User> userFromDatabase = userRepository.findFirstByLoginOrEmail(userName, userName);

        return userFromDatabase.map(user -> {
            if (!user.isActivated()) {
                throw new ApplicationException(ExceptionCode.USER_NOT_ACTIVATED, "User [" + user.getLogin() + "] was not activated");
            }

            if (user.isBlocked()) {
                throw new ApplicationException(ExceptionCode.USER_IS_BLOCKED, "User [" + user.getLogin() + "] is blocked");
            }

            List<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
                    .map(authority -> new SimpleGrantedAuthority(authority.getName()))
                    .collect(Collectors.toList());

            return new org.springframework.security.core.userdetails.User(
                    user.getLogin(),
                    user.getPasswordHash(),
                    grantedAuthorities
            );
        }).orElseThrow(() -> new ApplicationException(ExceptionCode.BAD_CREDENTIALS, "User [" + userName + "] was not found in the database"));
    }
}
