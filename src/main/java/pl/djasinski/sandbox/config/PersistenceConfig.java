package pl.djasinski.sandbox.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import pl.djasinski.sandbox.security.SpringSecurityAuditorAware;

@Configuration
@EnableJpaAuditing
public class PersistenceConfig {

    @Bean
    SpringSecurityAuditorAware getSpringSecurityAuditorAware() {
        return new SpringSecurityAuditorAware();
    }
}
