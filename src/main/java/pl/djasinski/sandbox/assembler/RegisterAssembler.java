package pl.djasinski.sandbox.assembler;

import pl.djasinski.sandbox.assembler.common.AbstractAssembler;
import pl.djasinski.sandbox.controller.request.RegisterRequest;
import pl.djasinski.sandbox.model.User;

/**
 * Class RegisterAssembler
 *
 * @author Dominik Jasiński zdjasinski@gmail.com
 */
public class RegisterAssembler extends AbstractAssembler<User, RegisterRequest> {

    @Override
    public User assemblyToDbo(final RegisterRequest dto) {
        return modelMapper.map(dto, User.class);
    }

    @Override
    public RegisterRequest assemblyToDto(final User dbo) {
        return modelMapper.map(dbo, RegisterRequest.class);
    }
}
