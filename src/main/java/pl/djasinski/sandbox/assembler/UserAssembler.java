package pl.djasinski.sandbox.assembler;

import pl.djasinski.sandbox.DTO.UserDTO;
import pl.djasinski.sandbox.assembler.common.AbstractAssembler;
import pl.djasinski.sandbox.model.User;

/**
 * Class RegisterAssembler
 *
 * @author Dominik Jasiński zdjasinski@gmail.com
 */
public class UserAssembler extends AbstractAssembler<User, UserDTO> {

    @Override
    public User assemblyToDbo(final UserDTO dto) {
        return modelMapper.map(dto, User.class);
    }

    @Override
    public UserDTO assemblyToDto(final User dbo) {
        return modelMapper.map(dbo, UserDTO.class);
    }
}
