package pl.djasinski.sandbox.assembler.common;


import pl.djasinski.sandbox.assembler.RegisterAssembler;
import pl.djasinski.sandbox.assembler.UserAssembler;

import java.io.Serializable;

/**
 * Class Assemblers
 */
public final class Assemblers implements Serializable {

    private Assemblers() {
    }

    public static final RegisterAssembler REGISTER_ASSEMBLER = new RegisterAssembler();

    public static final UserAssembler USER_ASSEMBLER = new UserAssembler();

}
