package pl.djasinski.sandbox.security;

import org.springframework.data.domain.AuditorAware;
import pl.djasinski.sandbox.constants.Constants;

import java.util.Optional;

/**
 * Implementation of AuditorAware based on Spring Security.
 */
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        String userName = SecurityUtils.getCurrentUserLogin();
        var currentAuditor = (userName != null ? userName : Constants.SYSTEM_ACCOUNT);

        return Optional.of(currentAuditor);
    }
}
