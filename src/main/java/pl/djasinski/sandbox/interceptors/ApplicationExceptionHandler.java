package pl.djasinski.sandbox.interceptors;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.djasinski.sandbox.exception.ApplicationException;
import pl.djasinski.sandbox.exception.ExceptionCode;
import pl.djasinski.sandbox.exception.FieldError;
import pl.djasinski.sandbox.exception.ResponseError;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@Log4j2
@RestControllerAdvice
public class ApplicationExceptionHandler {

    private static final Map<String, HttpStatus> codeStatusMap = new HashMap<>();

    static {
        codeStatusMap.put(ExceptionCode.CANNOT_PROCESS_FILE, INTERNAL_SERVER_ERROR);
    }

    private final MessageSource messageSource;

    public ApplicationExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> defaultErrorHandler(HttpServletRequest req, Exception e) {
        log.error(e.getMessage(), e);

        HttpStatus status = null;
        String code;
        List<FieldError> fieldErrors = null;

        if (e instanceof InternalAuthenticationServiceException) {
            e = (Exception) e.getCause();
        }

        if (e instanceof AccessDeniedException) {
            status = FORBIDDEN;
            code = ExceptionCode.ACCESS_DENIED;
        } else if (e instanceof ApplicationException) {
            ApplicationException applicationException = (ApplicationException) e;
            status = codeStatusMap.get(applicationException.getCode());
            code = applicationException.getCode();
        } else if (e instanceof MethodArgumentNotValidException) {
            code = ExceptionCode.VALIDATION_ERROR;
            BindingResult result = ((MethodArgumentNotValidException) e).getBindingResult();
            fieldErrors = processFieldErrors(result.getFieldErrors());
        } else {
            status = INTERNAL_SERVER_ERROR;
            code = ExceptionCode.DEFAULT_ERROR;
        }

        String description = null;

        try {
            description = messageSource.getMessage(code, null, Locale.getDefault());
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }

        ResponseError responseError = new ResponseError(code, description, fieldErrors);

        return ResponseEntity
                .status(Optional.ofNullable(status).orElse(BAD_REQUEST))
                .body(responseError);
    }

    private List<FieldError> processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors) {
        return fieldErrors.stream().map(fieldError ->
                new FieldError(fieldError.getField(), fieldError.getCode(), fieldError.getDefaultMessage())
        ).collect(Collectors.toList());
    }
}
