package pl.djasinski.sandbox.controller.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.djasinski.sandbox.controller.BaseController;

@RestController
@RequestMapping(PublicController.URL)
public class PublicController extends BaseController {

    protected static final String URL = "/api/v1/public";

    @GetMapping()
    public ResponseEntity<?> publicApi() {
        logCalledMethod();

        return ResponseEntity.ok().build();
    }
}
