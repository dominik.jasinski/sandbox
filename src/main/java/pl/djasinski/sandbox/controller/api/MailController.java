package pl.djasinski.sandbox.controller.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.djasinski.sandbox.controller.BaseController;
import pl.djasinski.sandbox.controller.request.MailRequest;
import pl.djasinski.sandbox.service.IMailService;

@RestController
@RequestMapping(MailController.URL)
public class MailController extends BaseController {

    protected static final String URL = "/api/mail";
    private final String SEND_REQUEST = "/sendRequest";

    private final IMailService mailService;

    public MailController(IMailService mailService) {
        this.mailService = mailService;
    }

    @PostMapping(SEND_REQUEST)
    public ResponseEntity<?> sendRequest(MailRequest mailRequest) {
        mailService.sendRequest(mailRequest);

        return ResponseEntity.ok().build();
    }
}
