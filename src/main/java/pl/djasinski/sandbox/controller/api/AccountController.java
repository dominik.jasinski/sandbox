package pl.djasinski.sandbox.controller.api;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.djasinski.sandbox.controller.BaseController;
import pl.djasinski.sandbox.controller.request.AuthenticationRequest;
import pl.djasinski.sandbox.controller.request.RegisterRequest;
import pl.djasinski.sandbox.controller.request.RemindPasswordRequest;
import pl.djasinski.sandbox.controller.request.ResetPasswordRequest;
import pl.djasinski.sandbox.controller.response.AuthenticationResponse;
import pl.djasinski.sandbox.service.IUserService;

import javax.validation.Valid;

@Log4j2
@RestController
@RequestMapping(AccountController.URL)
public class AccountController extends BaseController {

    protected static final String URL = "/api/v1/account";

    private final String AUTHENTICATE = "/authenticate";
    private final String REGISTER = "/register";
    private final String ACTIVATE = "/activate";
    private final String REMIND_PASSWORD = "/remind-password";
    private final String RESET_PASSWORD_START = "/reset-password/{token}";
    private final String RESET_PASSWORD_CONFIRM = "/reset-password";

    private final IUserService userService;

    public AccountController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = AUTHENTICATE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> authenticate(@RequestBody AuthenticationRequest authenticationRequest) {
        AuthenticationResponse response = userService.authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        return ResponseEntity.ok(response);
    }

    @PostMapping(REGISTER)
    public ResponseEntity<?> register(@RequestBody @Valid RegisterRequest requestDTO) {
        userService.registerUser(requestDTO);

        return ResponseEntity.ok().build();
    }

    @GetMapping(ACTIVATE)
    public ResponseEntity<?> activate(@RequestParam String key) {
        userService.activateUser(key);

        return ResponseEntity.ok().build();
    }

    @PostMapping(REMIND_PASSWORD)
    public ResponseEntity<?> remindPassword(@Valid @RequestBody RemindPasswordRequest request) {
        userService.resetPassword(request.getEmail());

        return ResponseEntity.ok().build();
    }

    @GetMapping(RESET_PASSWORD_START)
    public ResponseEntity<?> startResetPassword(@PathVariable("token") String token) {
        userService.checkRegistrationToken(token);

        return ResponseEntity.ok().build();
    }

    @PostMapping(RESET_PASSWORD_CONFIRM)
    public ResponseEntity<?> resetPassword(@Valid @RequestBody ResetPasswordRequest request) {
        userService.resetPassword(request);

        return ResponseEntity.ok().build();
    }
}
