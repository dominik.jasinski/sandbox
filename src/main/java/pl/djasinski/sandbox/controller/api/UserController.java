package pl.djasinski.sandbox.controller.api;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.djasinski.sandbox.controller.BaseController;
import pl.djasinski.sandbox.service.UserService;

@RestController
@RequestMapping(UserController.URL)
public class UserController extends BaseController {

    protected static final String URL = "/api/v1/user";

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping()
    public ResponseEntity<?> getUsers() {
        logCalledMethod();

        return ResponseEntity.ok(userService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable Long id) {
        logCalledMethod();

        return ResponseEntity.ok(userService.getOne(id));
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> removeUser(@PathVariable Long id) {
        logCalledMethod();

        userService.delete(id);

        return ResponseEntity.ok().build();
    }
}
