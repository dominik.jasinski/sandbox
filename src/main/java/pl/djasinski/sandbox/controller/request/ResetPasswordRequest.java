package pl.djasinski.sandbox.controller.request;

import lombok.Getter;
import lombok.Setter;
import pl.djasinski.sandbox.addnotation.FieldMatch;
import pl.djasinski.sandbox.constants.Constants;

import javax.validation.constraints.Pattern;

@Getter
@Setter
@FieldMatch(first = "password", second = "confirmPassword")
public class ResetPasswordRequest {

    @Pattern(regexp = Constants.PASSWORD_REGEX, message = "{validation.error.user.password}")
    private String password;

    @Pattern(regexp = Constants.PASSWORD_REGEX, message = "{validation.error.user.password}")
    private String confirmPassword;

    private String token;

}
