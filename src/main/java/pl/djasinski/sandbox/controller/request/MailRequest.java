package pl.djasinski.sandbox.controller.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MailRequest {

    @NotBlank
    private String user;
    @NotBlank
    @Email
    private String email;
    @NotBlank
    private String content;

}
