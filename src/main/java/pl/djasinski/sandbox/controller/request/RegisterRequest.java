package pl.djasinski.sandbox.controller.request;

import lombok.Getter;
import lombok.Setter;
import pl.djasinski.sandbox.addnotation.FieldMatch;
import pl.djasinski.sandbox.constants.Constants;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@FieldMatch(first = "password", second = "confirmPassword")
public class RegisterRequest {

    @Pattern(regexp = Constants.LOGIN_REGEX)
    @NotBlank
    private String login;

    //"Pole musi zostać wypełnione"
    @Pattern(regexp = Constants.EMAIL_REGEX)
    private String email;

    @Pattern(regexp = Constants.FIRST_NAME_REGEX, message = "{validation.error.user.firstName}")
    private String firstName;

    @Pattern(regexp = Constants.LAST_NAME_REGEX, message = "{validation.error.user.lastName}")
    private String lastName;

    @Pattern(regexp = Constants.PASSWORD_REGEX, message = "{validation.error.user.password}")
    private String password;

    @Pattern(regexp = Constants.PASSWORD_REGEX, message = "{validation.error.user.password}")
    private String confirmPassword;

}
