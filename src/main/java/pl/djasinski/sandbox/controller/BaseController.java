package pl.djasinski.sandbox.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Log4j2
public abstract class BaseController {

    public static String getBaseUrl() {
        return getBaseUrl("/");
    }

    public static String getBaseUrl(String path) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(path).build().toUriString();
    }

    protected void logCalledMethod() {
        if (log.isDebugEnabled()) {
            log.debug(String.format("%s.%s() - %s",
                    getClass().getSimpleName(),
                    Thread.currentThread().getStackTrace()[2].getMethodName(),
                    getBaseUrl()
            ));
        }
    }
}
