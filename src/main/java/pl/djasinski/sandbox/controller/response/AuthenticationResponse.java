package pl.djasinski.sandbox.controller.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class AuthenticationResponse {

    private final String jwt;
    private final Set<String> roles;

}
