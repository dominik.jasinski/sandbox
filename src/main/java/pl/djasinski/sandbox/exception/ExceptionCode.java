package pl.djasinski.sandbox.exception;

public final class ExceptionCode {
    public static final String DEFAULT_ERROR = "error.default";
    public static final String INCORRECT_FILE_FORMAT = "INCORRECT_FILE_FORMAT";
    public static final String INCORRECT_FILE_CONTENT = "INCORRECT_FILE_CONTENT";
    public static final String CANNOT_PROCESS_FILE = "CANNOT_PROCESS_FILE";
    public static final String NO_RESULTS_FOUND = "NO_RESULTS_FOUND";
    public static final String BAD_REQUEST = "BAD_REQUEST";
    public static final String ACCESS_DENIED = "error.access-denied";
    public static final String BAD_CREDENTIALS = "validation.error.user.authorization.bad-credentials";
    public static final String VALIDATION_ERROR = "validation.error";
    public static final String BAD_PASSWORD_RESET_TOKEN = "validation.error.user.passwordResetToken";
    public static final String USER_LOGIN_EXISTS = "validation.error.user.loginExists";
    public static final String USER_EMAIL_EXISTS = "validation.error.user.emailExists";
    public static final String USER_EMAIL_NOT_EXISTS = "validation.error.user.emailNotExists";
    public static final String USER_ACTIVATION_BAD_TOKEN = "validation.error.user.activation.badToken";
    public static final String USER_NOT_ACTIVATED = "validation.error.user.inactive";
    public static final String USER_IS_BLOCKED = "validation.error.user.blocked";

    private ExceptionCode() {
    }
}
