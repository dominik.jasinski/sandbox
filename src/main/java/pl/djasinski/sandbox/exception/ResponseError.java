package pl.djasinski.sandbox.exception;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class ResponseError {
    private String code;
    private String description;
    private List<FieldError> fieldErrors;
}
