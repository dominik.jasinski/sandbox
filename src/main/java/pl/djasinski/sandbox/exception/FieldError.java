package pl.djasinski.sandbox.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FieldError {
    private String fieldName;
    private String code;
    private String message;
}
