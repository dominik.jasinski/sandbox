package pl.djasinski.sandbox.constants;

public final class Constants {

    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";
    public static final String EMAIL_REGEX = "^[\\w\\.]+@[\\w]+\\.[\\w]+(\\.[a-z]{2,3})?$";
    public static final String FIRST_NAME_REGEX = "^[\\p{Lu}][\\p{Ll}]{2,}$";
    public static final String LAST_NAME_REGEX = "^[\\p{Lu}][\\p{Ll}]{2,}(-[\\p{Lu}][\\p{Ll}]{2,})?$";
    public static final String PASSWORD_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*";

    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";

    public static final String SYSTEM_ACCOUNT = "system";

    private Constants() {
    }
}
