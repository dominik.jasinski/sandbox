INSERT INTO authority (name) VALUES ('ROLE_ADMIN');
INSERT INTO authority (name) VALUES ('ROLE_USER');

-- INSERT INTO "user" (id, created_by, created_date, login, password_hash, activated)
-- VALUES (1, 'system', CURRENT_DATE, 'admin', '$2a$10$JP1Mv.7S6bZRYdmDldtgE.X/HmBjrH.X.NXZYZ1YsVlkJFBev9NQm', true);
--
-- INSERT INTO user_authority (user_id, authority_name) VALUES (1, 'ROLE_ADMIN');
-- INSERT INTO user_authority (user_id, authority_name) VALUES (1, 'ROLE_USER');