./build_docker.sh

docker stop sandbox-backend || true
docker rm sandbox-backend || true

echo "Backend started at http://localhost:8080"

docker container run -d \
 --network=sandbox-network-database \
 -p 8080:8080 \
 --mount 'src=backend-logs,dst=/app/logs' \
 --name sandbox-backend \
 sandbox-backend:latest
